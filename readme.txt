INTRODUCTION
------------

The Backbone Lib module adds the backbone.js library and inserts it onto specified paths.

REQUIREMENTS
------------
This module requires the following modules:
 * Libraries (https://www.drupal.org/project/libraries)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Download backbone.js from http://backbonejs.org/, extract and put backbone.js and backbone-min.js in libraries/backbone, so the actual libraries are available at libraries/backbone/backbone-min.js and libraries/backbone/backbone.js
   
CONFIGURATION
-------------
 * Visit admin/config/development/backbone-lib and choose the production (compressed) or development version of the backbone library. Note in order to use backbone.js underscore.js must also be installed on your system.
